<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAuthorizePlacesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authorize_places', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('authorize_type_id')->unsigned();
            $table->string('name');
            $table->string('address');
            $table->string('contact_phone');
            $table->string('email');
            $table->string('website');
            $table->float('latitude');
            $table->float('longitude');
            $table->string('logo');
            $table->tinyInteger('active')->default(0);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('authorize_places');
    }
}
