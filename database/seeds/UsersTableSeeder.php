<?php

use App\Repositories\UserRepository;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /** @var UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $message = "\n- No users were initialized. \n";
        foreach (config('setting.static_data.users') as $userData) {
            $queryUser = $this->userRepository->findWhere(['email' => $userData['email']])->first();
            if (empty($queryUser)) {
                $userData['password'] = bcrypt($userData['password']);
                $this->userRepository->create($userData);
                $message = "\n- Imported user: " . $userData['name'] . ' => ' . $userData['email'] . "\n";
            }
        }
        echo $message;
    }
}
