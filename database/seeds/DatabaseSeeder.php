<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ReportTypesTableSeeder::class);
        $this->call(AuthorizeTypesTableSeeder::class);
        $this->call(AuthorizePlacesTableSeeder::class);
        $this->call(ReportStatusesTableSeeder::class);
        $this->call(AuthorizeReportsTableSeeder::class);
    }
}