<?php

use App\Repositories\ReportTypeRepository;
use Illuminate\Database\Seeder;

class ReportTypesTableSeeder extends Seeder
{

    /** @var ReportTypeRepository */
    private $reportTypeRepository;

    public function __construct(ReportTypeRepository $reportTypeRepository)
    {
        $this->reportTypeRepository = $reportTypeRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $message = '';
        $imported = false;
        foreach (config('setting.static_data.report_types') as $reportType) {
            $queryObject = $this->reportTypeRepository->findWhere(['name' => $reportType['name']])->first();
            if (empty($queryObject)) {
                $imported = true;
                $this->reportTypeRepository->create($reportType);
                $message .= "\n- Imported report type: " . $reportType['name'] . "\n";
            }
        }

        if (!$imported) {
            $message = "\n- No report type were initialized. \n";
        }

        echo $message;
    }
}
