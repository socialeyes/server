<?php

use App\Repositories\AuthorizeReportRepository;
use Illuminate\Database\Seeder;

class AuthorizeReportsTableSeeder extends Seeder
{

    /** @var AuthorizeReportRepository */
    private $authorizeReportRepository;

    public function __construct(AuthorizeReportRepository $authorizeReportRepository)
    {
        $this->authorizeReportRepository = $authorizeReportRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $message = '';
        $imported = false;
        foreach (config('setting.static_data.authorize_reports') as $authorizeReport) {
            $queryObject = $this->authorizeReportRepository->findWhere([
                'authorize_place_id' => $authorizeReport['authorize_place_id'],
                'report_type_id' => $authorizeReport['report_type_id']
            ])->first();
            if (empty($queryObject)) {
                $imported = true;
                $this->authorizeReportRepository->create($authorizeReport);
                $message .= "\n- Imported authorize report: " . $authorizeReport['authorize_place_id'] . " & "  . $authorizeReport['authorize_place_id'] . "\n";
            }
        }

        if (!$imported) {
            $message = "\n- No authorize report were initialized. \n";
        }

        echo $message;
    }
}
