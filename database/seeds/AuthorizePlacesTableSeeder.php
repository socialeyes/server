<?php

use App\Repositories\AuthorizePlaceRepository;
use App\Repositories\ReportTypeRepository;
use Illuminate\Database\Seeder;

class AuthorizePlacesTableSeeder extends Seeder
{

    /** @var AuthorizePlaceRepository */
    private $authorizePlaceRepository;

    public function __construct(AuthorizePlaceRepository $authorizePlaceRepository)
    {
        $this->authorizePlaceRepository = $authorizePlaceRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $message = '';
        $imported = false;
        foreach (config('setting.static_data.authorize_places') as $authorizePlace) {
            $queryObject = $this->authorizePlaceRepository->findWhere(['name' => $authorizePlace['name']])->first();
            if (empty($queryObject)) {
                $imported = true;
                $this->authorizePlaceRepository->create($authorizePlace);
                $message .= "\n- Imported authorize place: " . $authorizePlace['name'] . "\n";
            }
        }

        if (!$imported) {
            $message = "\n- No authorize place were initialized. \n";
        }

        echo $message;
    }
}
