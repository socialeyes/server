<?php

use App\Repositories\AuthorizeTypeRepository;
use Illuminate\Database\Seeder;

class AuthorizeTypesTableSeeder extends Seeder
{

    /** @var AuthorizeTypeRepository */
    private $authorizeTypeRepository;

    public function __construct(AuthorizeTypeRepository $authorizeTypeRepository)
    {
        $this->authorizeTypeRepository = $authorizeTypeRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $message = '';
        $imported = false;
        foreach (config('setting.static_data.authorize_types') as $authorizeType) {
            $queryObject = $this->authorizeTypeRepository->findWhere(['name' => $authorizeType['name']])->first();
            if (empty($queryObject)) {
                $imported = true;
                $this->authorizeTypeRepository->create($authorizeType);
                $message .= "\n- Imported authorize type: " . $authorizeType['name'] . "\n";
            }
        }

        if (!$imported) {
            $message = "\n- No authorize type were initialized. \n";
        }

        echo $message;
    }
}
