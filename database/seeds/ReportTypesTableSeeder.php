<?php

use App\Repositories\ReportStatusRepository;
use Illuminate\Database\Seeder;

class ReportStatusesTableSeeder extends Seeder
{

    /** @var ReportStatusRepository */
    private $reportStatusRepository;

    public function __construct(ReportStatusRepository $reportStatusRepository)
    {
        $this->reportStatusRepository = $reportStatusRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $message = '';
        $imported = false;
        foreach (config('setting.static_data.report_statuses') as $reportType) {
            $queryObject = $this->reportStatusRepository->findWhere(['name' => $reportType['name']])->first();
            if (empty($queryObject)) {
                $imported = true;
                $this->reportStatusRepository->create($reportType);
                $message .= "\n- Imported report status: " . $reportType['name'] . "\n";
            }
        }

        if (!$imported) {
            $message = "\n- No report status were initialized. \n";
        }

        echo $message;
    }
}
