<?php

return [
    'static_data' => [
        'api_token' => env('API_TOKEN', '8f7484fdd17b6ea927e2fdfe99e34711716c04159b606f8c0f7626039a39ec7181a4395d9f59915676faf55f07687e0c59ea'),
        'sex' => [
            'Other' => 'Other',
            'M' => 'M',
            'F' => 'F'
        ],
        'users' => [
            'admin' => [
                'name' => 'Admin',
                'email' => 'tnguonchhay@gmail.com',
                'password' => 'admin'
            ]
        ],
        'report_types' => [
            [
                'name' => 'Accident',
                'image' => 'storage/report_type/accident.png'
            ],
            [
                'name' => 'Traffic Jam',
                'image' => 'storage/report_type/traffic_jam.png'
            ],
            [
                'name' => 'Violence',
                'image' => 'storage/report_type/violence.png'
            ],
            [
                'name' => 'Robbery',
                'image' => 'storage/report_type/robbery.png'
            ]
        ],
        'authorize_types' => [
            ['name' => 'Police'],
            ['name' => 'Traffic police'],
            ['name' => 'Hospital'],
            ['name' => 'NGO']
        ],
        'authorize_places' => [
            [
                'authorize_type_id' => 1,
                'name' => 'P. Dimang',
                'address' => 'Dimang address',
                'logo' => 'storage/authorize_place/dimang.jpg',
                'contact_phone' => '+48 884918826',
                'email' => 'dimang@gmail.com',
                'website' => 'http://www.dimang.info',
                'latitude' => 51.2346329,
                'longitude' => 22.5465373,
                'active' => 1
            ],
            [
                'authorize_type_id' => 2,
                'name' => 'TP. Sopheak',
                'address' => 'Sopheak address',
                'logo' => 'storage/authorize_place/sopheak.jpg',
                'contact_phone' => '+48 575 032 662',
                'email' => 'sopheak@gmail.com',
                'website' => 'http://www.sopheak.info',
                'latitude' => 51.2377603,
                'longitude' => 22.545063,
                'active' => 1
            ],
            [
                'authorize_type_id' => 3,
                'name' => 'H. Akphecheat',
                'address' => 'Akphecheat address',
                'logo' => 'storage/authorize_place/akphecheat.jpg',
                'contact_phone' => '+48 733 939 336',
                'email' => '',
                'website' => 'http://www.akphecheat.info',
                'latitude' => 51.2508655,
                'longitude' => 22.4936642,
                'active' => 1
            ],
            [
                'authorize_type_id' => 4,
                'name' => 'N. Puthea',
                'address' => 'Puthea address',
                'logo' => 'storage/authorize_place/puthea.jpg',
                'contact_phone' => '+48 791 855 220',
                'email' => 'puthea@gmail.com',
                'website' => 'http://www.puthea.com',
                'latitude' => 51.2586562,
                'longitude' => 22.4946715,
                'active' => 1
            ]
        ],
        'authorize_reports' => [
            [
                'authorize_place_id' => 1,
                'report_type_id' => 1
            ],
            [
                'authorize_place_id' => 1,
                'report_type_id' => 3
            ],
            [
                'authorize_place_id' => 1,
                'report_type_id' => 4
            ],

            [
                'authorize_place_id' => 2,
                'report_type_id' => 2
            ],

            [
                'authorize_place_id' => 3,
                'report_type_id' => 1
            ],
            [
                'authorize_place_id' => 3,
                'report_type_id' => 3
            ],

            [
                'authorize_place_id' => 4,
                'report_type_id' => 3
            ]
        ],
        'report_statuses' => [
            ['name' => 'Reporting'],
            ['name' => 'Took action'],
            ['name' => 'Resolved'],
            ['name' => 'Unresolved']
        ],
    ],
    'activation' => [
        'sender_address' => env('EMAIL_GATEWAY_ADDRESS', 'tnguonchhay@gmail.com'),
        'sender_name' => env('EMAIL_GATEWAY_NAME', 'SocialEye'),
        'registration' => [
            'template' => 'email_templates.reporter.send_activated_code',
            'subject' => 'Registration verification',
            'expire_activated_code' => 3600, // 3600 seconds = 60 minutes
            'code_length' => 6
        ],
        'expired_verify_code' => [
            'template' => 'email_templates.reporter.expired_verify_code',
            'subject' => 'New register verify code'
        ],
        'forgot_password' => [
            'template' => 'email_templates.reporter.send_reset_password_code',
            'subject' => 'Reset password code',
            'address' => env('EMAIL_GATEWAY_REGISTER_ADDRESS', 'tnguonchhay@gmail.com'),
            'code_length' => 6
        ]
    ]
];
