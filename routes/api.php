<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/**
 * API requests with API_Token
 */
Route::group(['middleware' => ['api', 'api_token'], 'prefix' => 'v1', 'namespace' => 'V1'], function () {

    Route::post('file_upload', 'FileAPIController@upload');

    Route::get('report_types', 'ReportTypeAPIController@index');

    Route::group(['prefix' => 'authorize_places'], function() {
        Route::get('', 'AuthorizePlaceAPIController@index');
    });

    Route::group(['prefix' => 'authorize_reports'], function() {
        Route::get('', 'AuthorizeReportAPIController@index');
    });

    Route::group(['prefix' => 'registers'], function() {
        Route::post('', 'RegisterAPIController@store');
        Route::post('verify_code', 'RegisterAPIController@verifyCode');
    });

    Route::group(['prefix' => 'authorization'], function() {
        Route::post('/login', 'LoginAPIController@login');
        Route::post('/send_reset_password_code', 'LoginAPIController@sendResetPasswordCode');
        Route::post('/reset_password', 'LoginAPIController@resetPassword');
    });
});

/**
 * API requests with Access_Token
 */
Route::group(['middleware' => ['api', 'access_token'], 'prefix' => 'v1', 'namespace' => 'V1'], function () {

    Route::group(['prefix' => 'authorization'], function() {
        Route::post('/logout', 'LoginAPIController@logout');
        Route::post('/check_user_session', 'LoginAPIController@checkUserSession');
    });

    Route::group(['prefix' => 'reports'], function() {
        Route::get('/{reporterId}', 'ReportAPIController@index');
        Route::post('/', 'ReportAPIController@store');
        Route::delete('/', 'ReportAPIController@delete');
    });

    Route::group(['prefix' => 'reporters'], function() {
        Route::put('/{id}/update_credential', 'ReporterAPIController@updateCredential');
        Route::put('/{id}/update_info', 'ReporterAPIController@updateInfo');
    });

});