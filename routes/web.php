<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Route::get('/reports/{id}/view', 'Frontend\ReportController@show');
Route::get('/', 'Frontend\HomeController@index')->name('homepage');
Route::get('/home', 'Frontend\HomeController@index');

Auth::routes();

/*
|--------------------------------------------------------------------------
| Admin routes
|--------------------------------------------------------------------------
*/

Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
    'middleware' => ['auth', 'web']
], function() {
    Route::get('/', 'DashboardController@index');
    Route::resource('users', 'UserController');
    Route::resource('report_types', 'ReportTypeController');
    Route::resource('reports', 'ReportController');
    Route::resource('authorizeTypes', 'AuthorizeTypeController');
    Route::resource('authorizePlaces', 'AuthorizePlaceController');
});