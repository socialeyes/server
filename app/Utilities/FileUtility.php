<?php

namespace App\Utilities;

use Illuminate\Support\Facades\Storage;

class FileUtility {

    /**
     * @param $filename
     *
     * @return void
     */
    public static function delete($filename)
    {
        if (file_exists($filename)) {
            unlink($filename);
        }
    }

    /**
     * A function to adjust the pre-update process of file by following steps:
     * 1. Pre-upload file to server. Server responses a temp filename
     * 2. Mobile upload a temp filename
     * 3. Server move the temp filename from temp to real path
     *
     * @param $tempFilenameAndPath
     * @param $path
     *
     * @return string
     */
    public static function saveUploadResource($tempFilenameAndPath, $path)
    {
        $filename = '';

        // $tempFilenameAndPath: http://domain.com/storage/temp/6zG4UkmrXDHgURGefUq81i3ScZ7dLVnCvLn3Zk9R.jpeg
        $temps = explode('/temp/', $tempFilenameAndPath);
        if (count($temps)) {
            // $tempFilename: 16zG4UkmrXDHgURGefUq81i3ScZ7dLVnCvLn3Zk9R.jpeg
            $tempFilename = end($temps);

            // $source: storage/temp/6zG4UkmrXDHgURGefUq81i3ScZ7dLVnCvLn3Zk9R.jpeg
            $source = 'storage/temp/' . $tempFilename;

            if (file_exists($source)) {
                $destination = $path . '/' . $tempFilename;
                copy($source, $destination);

                // Delete file from temp folder after success move
                FileUtility::delete($source);

                $filename = $destination;
            }
        }

        return $filename;
    }
}