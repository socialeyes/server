<?php

namespace App\Utility;

class EmailUtility
{

    /**
     * @param string $email
     *
     * @return bool
     */
    public static function isValidEmail($email)
    {
        if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        return false;
    }
}