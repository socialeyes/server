<?php
namespace App\Utilities;
use Illuminate\Http\JsonResponse;
use Response;

/**
 *  This class is belong to SportPill
 */

class ResponseUtility
{

    /**
     * @param string $message
     * @param mixed  $data
     * @param $version
     *
     * @return array
     */
    public static function makeResponse($message, $data, $version = 'v1') {
        return [
            'success' => true,
            'version' => $version,
            'data'    => $data,
            'message' => $message,
        ];
    }

    /**
     * @param string $message
     * @param $version
     * @param array  $data
     *
     * @return array
     */
    public static function makeError($message, $version = 'v1', array $data = [])
    {
        $response = [
            'success' => false,
            'version' => $version,
            'message' => $message,
        ];

        if (! empty($data)) {
            $response['data'] = $data;
        }

        return $response;
    }

    /**
     * @param $result
     * @param $message
     * @param string $version
     * @return Response
     */
    public static function sendResponse($result, $message, $version = 'v1')
    {
        return Response::json(ResponseUtility::makeResponse($message, $result, $version));
    }

    /**
     * @param $error
     * @param int $code
     * @param array $data
     * @param string $version
     * @return Response
     */
    public static function sendError($error, $code = 404, $data = [], $version = 'v1')
    {
        return Response::json(ResponseUtility::makeError($error, $version, $data), $code);
    }
}
