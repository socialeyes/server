<?php

namespace App\Http\Middleware;

use App\Utilities\ResponseUtility;
use Closure;

class APIToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $apiToken = (string) $request->bearerToken();
        $apiToken = trim($apiToken);
        if ($apiToken !== config('setting.static_data.api_token')) {
            return ResponseUtility::sendError('API token is incorrect.', 401);
        }

        /**
         * Continue the next request for valid request
         */
        return $next($request);
    }
}
