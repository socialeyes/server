<?php

namespace App\Http\Middleware;

use App\Models\Reporter;
use App\Utilities\ResponseUtility;
use Closure;
use SebastianBergmann\CodeCoverage\Report\Xml\Report;

class AccessToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $accessToken = (string) $request->bearerToken();
        $accessToken = trim($accessToken);

        /** @var Reporter|null $authorizeReporter */
        $authorizeReporter = Reporter::where('remember_token', $accessToken)->first();
        if (!($authorizeReporter instanceof Reporter)) {
            return ResponseUtility::sendError('Access token is incorrect.', 401);
        }

        if (!$authorizeReporter->isSessionValid()) {
            return ResponseUtility::sendError('Session is expired', 401);
        }

        /**
         * Continue the next request for valid request
         */
        return $next($request);
    }
}
