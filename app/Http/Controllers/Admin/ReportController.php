<?php

namespace App\Http\Controllers\Admin;

use App\Models\Report;
use App\Repositories\ReportDetailRepository;
use App\Repositories\ReportRepository;
use App\Repositories\ReportStatusRepository;
use Illuminate\Http\Request;
use Response;

class ReportController extends AdminBaseController
{

    /** @var ReportRepository */
    private $reportRepository;

    /** @var ReportStatusRepository */
    private $reportStatusRepository;

    /** @var ReportDetailRepository */
    private $reportDetailRepository;

    /**
     * @param ReportRepository $reportRepository
     * @param ReportStatusRepository $reportStatusRepository
     * @param ReportDetailRepository $reportDetailRepository
     */
    public function __construct(
        ReportRepository $reportRepository,
        ReportStatusRepository $reportStatusRepository,
        ReportDetailRepository $reportDetailRepository
    ) {
        parent::__construct();
        $this->activeMenu = ['main' => 'report', 'sub' => ''];
        $this->viewPath = 'admins.reports.';
        $this->routePath = 'reports.';
        $this->reportRepository = $reportRepository;
        $this->reportStatusRepository = $reportStatusRepository;
        $this->reportDetailRepository = $reportDetailRepository;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $reports = $this->reportRepository->all();
        return $this->assignToView('index', 'Report list', [
            'reports' => $reports
        ]);
    }

    /**
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $report = $this->reportRepository->findWithoutFail($id);
        if (empty($report)) {
            return $this->redirectToIndex();
        }

        $reportStatuses = $this->reportStatusRepository->pluck('name', 'id');
        return $this->assignToView('show', 'Report Detail', [
            'report' => $report,
            'reportStatuses' => $reportStatuses
        ]);
    }

    /**
     * @param int $id
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $reportDetail = $this->reportDetailRepository->findWithoutFail($id);
        if (empty($reportDetail)) {
           return $this->redirectToIndex();
        }

        /** @var Report $report */
        $report = $reportDetail->report;
        $reportStatusId = $request->get('report_status_id');
        $updateReportStatusData = [
            'report_status_id' => $reportStatusId
        ];

        // Update report detail satus
        $reportDetail = $this->reportDetailRepository->update($updateReportStatusData, $id);

        // Update report status
        $updateReportStatusData = [
            'report_status_id' => $report->getReportStatusIdToUpdate()
        ];
        $report = $this->reportRepository->update($updateReportStatusData, $report->id);

        return $this->redirectToIndex();
    }
}