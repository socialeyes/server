<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Response;

class AdminBaseController extends Controller
{
    /**
     * AppBaseController constructor.
     */
    public function __construct()
    {
		$this->middleware('auth');
    }

    /**
     * @param string $viewAction
     * @param string $pageTitle
     * @param array $values
     *
     * @return mixed
     */
    public function assignToView($viewAction, $pageTitle = '', $values = array())
    {
        $view = view($this->viewPath . $viewAction)
            ->with('pageTitle', $pageTitle)
            ->with('activeMenu', $this->activeMenu);

        foreach ($values as $key => $value) {
            $view = $view->with($key, $value);
        }

        return $view;
    }

    /**
     * @return Response
     */
    public function redirectToIndex()
    {
        return $this->redirectTo('index');
    }

    /**
     * @param $action
     * @return Response
     */
    public function redirectTo($action)
    {
        return redirect(route($this->routePath . $action));
    }
}
