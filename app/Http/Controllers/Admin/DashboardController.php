<?php
namespace App\Http\Controllers\Admin;

class DashboardController extends AdminBaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->activeMenu = ['main' => 'dashboard', 'sub' => ''];
        $this->viewPath = 'admins.dashboard.';
    }

    /**
     * @return Response
     */
    public function index()
    {
        return $this->assignToView('index', 'Dashboard');
    }
}
