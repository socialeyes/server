<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class UserController extends AdminBaseController
{
    /** @var UserRepository */
    private $userRepository;


    /**
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        parent::__construct();
        $this->activeMenu = ['main' => 'user', 'sub' => ''];
        $this->viewPath = 'admins.users.';
        $this->routePath = 'users.';
        $this->userRepository = $userRepository;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->userRepository->pushCriteria(new RequestCriteria($request));
        $users = $this->userRepository->all();

        return $this->assignToView('index', 'User list', [
            'users' => $users
        ]);
    }

    /**
     * @return Response
     */
    public function create()
    {
        return $this->assignToView('create', 'New user');
    }

    /**
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $input = $request->all();
        if (($input['confirm_password'] !== $input['password'])) {
            Flash::error('Password and Confirm password must be the same');
            return $this->assignToView('create', 'New user');
        } else {
            $input['password'] = bcrypt($input['password']);
        }

        $user =  $this->userRepository->create($input);
        Flash::success('User saved successfully.');
        return $this->redirectToIndex();
    }

    /**
     * @param string $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var User $user */
        $user =  $this->checkExistUser($id);

        return $this->assignToView('edit', 'Edit user', [
            'user' => $user
        ]);
    }

    /**
     * @param string $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        /** @var User $user */
        $user =  $this->checkExistUser($id);
        $input = $request->all();

        if (empty($user)) {
            Flash::error('User not found');
            return redirect(route($this->routePath . 'index'));
        }

        if ($input['password'] !== '' && $input['confirm_password'] !== $input['password']) {
            Flash::error('Password and Confirm password must be the same');
            return redirect(route('users.edit', ['id' => $user->id]));
        }

        if ($input['password'] !== '') {
            $input['password'] = bcrypt($input['password']);
        } else {
            unset($input['password']);
        }

        $user =  $this->userRepository->update($input, $id);
        Flash::success('User updated successfully.');
        return $this->redirectToIndex();
    }

    /**
     * @param string $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var User $user */
        $user =  $this->checkExistUser($id);

        $this->userRepository->delete($id);
        if (Auth::user()->id === $id) {
            return redirect('/logout');
        }

        Flash::success('User deleted successfully.');
        return $this->redirectToIndex();
    }

    /**
     * @param string $id
     *
     * @return User|\Response
     */
    public function checkExistUser($id)
    {
        /** @var User $user */
        $user =  $this->userRepository->findWithoutFail($id);
        if (empty($user)) {
            Flash::error('User not found');
            return $this->redirectToIndex();
        }
        return $user;
    }
}
