<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;

/**
 * @package App\Http\Controllers\API
 *
 * @SWG\Swagger(
 *   host="socialeye.reasei.co",
 *   basePath="/api/v1",
 *   @SWG\Info(
 *     title="Social Eye RESTful API docs",
 *     description="Base testing API url: `http://socialeye.reasei.co/api/v1`. We use `api_token` for general requests and `access_token` for authorize requests",
 *     version="1.0.0"
 *   )
 * )
 */
class BaseAPIController extends Controller
{
    public function __construct()
    {
        $this->middleware('api');
    }

    /**
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('reporters');
    }

    /**
     * @param Request $request
     * @return string
     */
    public function getAccessTokenFromRequest(Request $request)
    {
        $accessToken = (string) $request->bearerToken();
        $accessToken = trim($accessToken);
        return $accessToken;
    }
}
