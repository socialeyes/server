<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\API\BaseAPIController;
use App\Models\AuthorizePlace;
use App\Models\AuthorizeReport;
use App\Repositories\AuthorizePlaceRepository;
use App\Repositories\AuthorizeReportRepository;
use App\Utilities\ResponseUtility;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class AuthorizeReportAPIController extends BaseAPIController
{
    /** @var AuthorizeReportRepository */
    private $authorizeReportRepository;

    /**
     * @param AuthorizeReportRepository $authorizeReportRepository
     */
    public function __construct(AuthorizeReportRepository $authorizeReportRepository)
    {
        parent::__construct();
        $this->authorizeReportRepository = $authorizeReportRepository;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/authorize_reports",
     *      summary="Get a listing of the authorize reports.",
     *      tags={"AuthorizeReport"},
     *      description="Get a listing of the authorize reports.",
     *      produces={"application/json"},
     *
     *     @SWG\Parameter(
     *          name="api_token",
     *          description="api token of server",
     *          type="string",
     *          required=true,
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="report_type_id",
     *          in="formData",
     *          description="report_type_id",
     *          required=false,
     *          type="string"
     *      ),
     *
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/AuthorizeReport")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $authorizeReports = [];
        if ($request->has('report_type_id')) {
            $authorizeReports = $this->authorizeReportRepository->with([])->findWhere([
                'report_type_id' => $request->get('report_type_id')
            ]);
        } else {
            $authorizeReports = $this->authorizeReportRepository->all();
        }

        $responseData = AuthorizeReport::getResponseData($authorizeReports);
        return ResponseUtility::sendResponse($responseData, 'AuthorizeReport retrieved successfully');
    }
}
