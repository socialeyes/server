<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\API\BaseAPIController;
use App\Models\Reporter;
use App\Repositories\ReporterRepository;
use App\Utilities\FileUtility;
use App\Utilities\ResponseUtility;
use Illuminate\Http\Request;
use Response;

class ReporterAPIController extends BaseAPIController
{
    /** @var ReporterRepository */
    private $reporterRepository;

    /**
     * @param ReporterRepository $reporterRepository
     */
    public function __construct(ReporterRepository $reporterRepository)
    {
        parent::__construct();
        $this->reporterRepository = $reporterRepository;
    }

    /**
     * @param int $id
     * @param Request $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/reporters/{id}/update_credential",
     *      summary="Update reporter credential",
     *      tags={"Reporter"},
     *      description="Update reporter credential",
     *      produces={"application/json"},
     *
     *     @SWG\Parameter(
     *          name="access_token",
     *          description="Access token",
     *          type="string",
     *          required=true,
     *          in="header"
     *      ),
     *
     *      @SWG\Parameter(
     *          name="password",
     *          description="Reporter new password",
     *          type="string",
     *          required=true,
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="confirm_password",
     *          description="Reporter confirm_password",
     *          type="string",
     *          required=true,
     *          in="formData"
     *      ),
     *
     *      @SWG\Response(
     *          response=200,
     *          description="Credential was updated successfully.",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Reporter")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=400,
     *          description="Fields are required."
     *      ),
     *      @SWG\Response(
     *          response=404,
     *          description="Reporter was not found."
     *      )
     * )
     */
    public function updateCredential($id, Request $request)
    {
        /** @var Reporter $reporter */
        $reporter = $this->reporterRepository->findWithoutFail($id);
        if (empty($reporter)) {
            return ResponseUtility::sendError('Reporter does not found.', 422);
        }

        if (!$request->has('password') || !$request->has('confirm_password')) {
            return ResponseUtility::sendError('Fields are required.', 400);
        }

        $password = trim($request->get('password'));
        $confirmPassword = trim($request->get('confirm_password'));
        if ($password !== $confirmPassword) {
            return ResponseUtility::sendError('Password and confirm password must be the same.', 422);
        } else {
            $reporterUpdateData['password'] = bcrypt($password);
        }

        $reporter = $this->reporterRepository->update($reporterUpdateData, $id);
        return ResponseUtility::sendResponse($reporter->getSingleResponseData(), 'Credential was updated successfully.');
    }

    /**
     * @param int $id
     * @param Request $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/reporters/{id}/update_info",
     *      summary="Update reporter basic information",
     *      tags={"Reporter"},
     *      description="Update reporter basic information",
     *      produces={"application/json"},
     *
     *     @SWG\Parameter(
     *          name="access_token",
     *          description="Access token",
     *          type="string",
     *          required=true,
     *          in="header"
     *      ),
     *
     *      @SWG\Parameter(
     *          name="sex",
     *          description="Reporter sex",
     *          type="string",
     *          required=true,
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="birth_date",
     *          description="Reporter birth date",
     *          type="string",
     *          required=true,
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="address",
     *          description="Reporter address",
     *          type="string",
     *          required=true,
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="image",
     *          description="Reporter image. Image file path from the pre-upload request",
     *          type="string",
     *          required=false,
     *          in="formData"
     *      ),
     *     @SWG\Parameter(
     *          name="first_name",
     *          description="Reporter first name",
     *          type="string",
     *          required=false,
     *          in="formData"
     *      ),
     *
     *     @SWG\Parameter(
     *          name="last_name",
     *          description="Reporter last name",
     *          type="string",
     *          required=false,
     *          in="formData"
     *      ),
     *
     *      @SWG\Response(
     *          response=200,
     *          description="Basic information was updated successfully.",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Reporter")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=400,
     *          description="Fields are required."
     *      ),
     *      @SWG\Response(
     *          response=404,
     *          description="Reporter was not found."
     *      )
     * )
     */
    public function updateInfo($id, Request $request)
    {
        /** @var Reporter $reporter */
        $reporter = $this->reporterRepository->findWithoutFail($id);
        if (empty($reporter)) {
            return ResponseUtility::sendError('Reporter does not found.', 422);
        }

        if (!$request->has('sex') || !$request->has('birth_date') || !$request->has('address')) {
            return ResponseUtility::sendError('Fields are required.', 400);
        }

        $reporterUpdateData = [
            'sex' => $request->get('sex'),
            'birth_date' => $request->get('birth_date'),
            'address' => $request->get('address')
        ];

        if ($request->has('image')) {
            $image = $request->get('image') . '';
            if ($image !== '') {
                $path = str_replace('public/', 'storage/', Reporter::IMAGE_STORAGE_PATH);
                $reporterUpdateData['image'] = FileUtility::saveUploadResource($image, $path);
            }
        }

        if ($request->has('first_name')) {
            $firstName =  $request->get('first_name') . '';
            if ($firstName !== '') {
                $reporterUpdateData['first_name'] = $request->get('first_name');
            }
        }

        if ($request->has('last_name')) {
            $lastName = $request->get('last_name') . '';
            if ($lastName !== '') {
                $reporterUpdateData['last_name'] = $request->get('last_name');
            }
        }

        $reporter = $this->reporterRepository->update($reporterUpdateData, $id);
        return ResponseUtility::sendResponse($reporter->getSingleResponseData(), 'Basic information was updated successfully.');
    }
}