<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\API\BaseAPIController;
use App\Models\Reporter;
use App\Repositories\ReporterRepository;
use App\Utility\EmailUtility;
use Illuminate\Http\Request;
use App\Utilities\ResponseUtility;
use Response;
use Validator;

class RegisterAPIController extends BaseAPIController
{
    /** @var ReporterRepository */
    private $reporterRepository;

    /**
     * @param ReporterRepository $reporterRepository
     */
    public function __construct(ReporterRepository $reporterRepository)
    {
        parent::__construct();
        $this->reporterRepository = $reporterRepository;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/registers",
     *      summary="Register new reporter",
     *      tags={"Register"},
     *      description="Register new reporter",
     *      produces={"application/json"},
     *
     *      @SWG\Parameter(
     *          name="api_token",
     *          description="API token of server",
     *          type="string",
     *          required=true,
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="email",
     *          description="Email for login",
     *          type="string",
     *          required=true,
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="password",
     *          description="Password for login",
     *          type="string",
     *          required=true,
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="address",
     *          description="User territory address",
     *          type="string",
     *          required=true,
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="birth_date",
     *          description="Date of birth of user. Ex: yyyy.mm.dd",
     *          type="string",
     *          required=true,
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="sex",
     *          description="Gender of user: Other, M, F",
     *          type="string",
     *          required=true,
     *          in="formData"
     *      ),
     *
     *      @SWG\Response(
     *          response=200,
     *          description="successful registration",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Reporter")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=400,
     *          description="Fields are required. OR Email is invalid."
     *      ),
     *      @SWG\Response(
     *          response=422,
     *          description="Email already existed."
     *      )
     * )
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, Reporter::$rules);
        if ($validator->fails()) {
            return ResponseUtility::sendError('Fields are required.', 400);
        }

        /**
         * Validate email format
         */
        if (!EmailUtility::isValidEmail($input['email'])) {
            return ResponseUtility::sendError('Email is invalid.', 400);
        }

        /** @var Reporter|null $reporter */
        $queryReporter = $this->reporterRepository->findWhere(['email' => $input['email']])->first();
        if (!empty($queryReporter)) {
            return ResponseUtility::sendError('Email already existed.', 422, $queryReporter->getSingleResponseData());
        }

        /**
         * Send verify code to register user
         */
        $registerConfiguration =  config('setting.activation.registration');
        $verifyCode = Reporter::generateVerifyCode(intval($registerConfiguration['code_length']));
        $registerConfiguration['email'] = $input['email'];
        $registerConfiguration['verify_code'] = $verifyCode;
        Reporter::sendVerificationCodeToEmail($registerConfiguration);

        /**
         * Register new user
         */
        $input['remember_token'] = $verifyCode;
        $input['password'] = bcrypt(trim($input['password']));
        $reporter = $this->reporterRepository->create($input);
        return ResponseUtility::sendResponse($reporter->getSingleResponseData(), 'Reporter was created successfully');
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/registers/verify_code",
     *      summary="Verify code",
     *      tags={"Register"},
     *      description="Verify code after registration",
     *      produces={"application/json"},
     *
     *      @SWG\Parameter(
     *          name="api_token",
     *          description="API token of server",
     *          type="string",
     *          required=true,
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="reporter_id",
     *          in="formData",
     *          description="Registered reporter id",
     *          required=true,
     *          type="integer"
     *      ),
     *      @SWG\Parameter(
     *          name="verify_code",
     *          in="formData",
     *          description="sent verification code",
     *          required=true,
     *          type="string"
     *      ),
     *
     *      @SWG\Response(
     *          response=200,
     *          description="successful verification the code",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Reporter")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=400,
     *          description="Fields are required."
     *      ),
     *      @SWG\Response(
     *          response=304,
     *          description="Verify code is incorrect."
     *      ),
     *      @SWG\Response(
     *          response=404,
     *          description="Reporter was not found."
     *      ),
     *      @SWG\Response(
     *          response=422,
     *          description="Reporter was already activated."
     *      )
     * )
     */
    public function verifyCode(Request $request)
    {
        if (!$request->has('reporter_id') || !$request->has('verify_code')) {
            return ResponseUtility::sendError('Fields are required.', 400, $request->all());
        }

        /** @var Reporter|null $reporter */
        $reporter = $this->reporterRepository->findWithoutFail($request->get('reporter_id'));
        if (empty($reporter)) {
            return ResponseUtility::sendError('Reporter was not found.', 404, [
                'id' => $request->get('reporter_id')
            ]);
        }

        if (intval($reporter->activate) != 0) {
            return ResponseUtility::sendError('Reporter was already activated.', 422, $reporter->getSingleResponseData());
    }

        /**
         * Check verify code expiration
         */
        $verifyCodeExpireDuration = intval(config('setting.activation.registration.expire_activated_code'));
        if (!Reporter::isVerificationPeriodValid($reporter, $verifyCodeExpireDuration)) {
            /**
             * Resend verify code to register user
             */
            $registerConfiguration =  config('setting.activation.registration');
            $verifyCode = Reporter::generateVerifyCode(intval($registerConfiguration['code_length']));
            $verifyCodeConfiguration =  config('setting.activation.expired_verify_code');
            $verifyCodeConfiguration['expire_activated_code'] = $registerConfiguration['expire_activated_code'];
            $verifyCodeConfiguration['email'] = $reporter->email;
            $verifyCodeConfiguration['verify_code'] = $verifyCode;
            Reporter::sendVerificationCodeToEmail($verifyCodeConfiguration);

            $updateData = [
                'remember_token' => $verifyCode

            ];
            $reporter = $this->reporterRepository->update($updateData, $reporter->id);
            return ResponseUtility::sendError('Verify code was expired. We sent you a new verify code. Please check again.', 408, [
                'id' => $request->get('reporter_id')
            ]);
        }

        if ($reporter->remember_token != trim($request->get('verify_code'))) {
            return ResponseUtility::sendError('Verify code is incorrect.', 442, ['verify_code' => $request->get('verify_code')]);
        }

        $updateData = [
            'activate' => 1,
            'remember_token' => ''

        ];
        $reporter = $this->reporterRepository->update($updateData, $reporter->id);

        return ResponseUtility::sendResponse($reporter->getSingleResponseData(), 'Reporter was activated successfully');
    }
}
