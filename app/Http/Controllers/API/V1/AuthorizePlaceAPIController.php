<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\API\BaseAPIController;
use App\Models\AuthorizePlace;
use App\Repositories\AuthorizePlaceRepository;
use App\Utilities\ResponseUtility;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class AuthorizePlaceAPIController extends BaseAPIController
{
    /** @var  AuthorizePlaceRepository */
    private $authorizePlaceRepository;

    /**
     * @param AuthorizePlaceRepository $authorizePlaceRepository
     */
    public function __construct(AuthorizePlaceRepository $authorizePlaceRepository)
    {
        parent::__construct();
        $this->authorizePlaceRepository = $authorizePlaceRepository;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/authorize_places",
     *      summary="Get a listing of the authorize places.",
     *      tags={"AuthorizePlace"},
     *      description="Provide `api_token` in Header request",
     *      produces={"application/json"},
     *
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/AuthorizePlace")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->authorizePlaceRepository->pushCriteria(new RequestCriteria($request));
        $this->authorizePlaceRepository->pushCriteria(new LimitOffsetCriteria($request));
        $authorizePlaces = $this->authorizePlaceRepository->all();

        return ResponseUtility::sendResponse(AuthorizePlace::getAllResponseData($authorizePlaces), 'AuthorizePlace retrieved successfully');
    }
}
