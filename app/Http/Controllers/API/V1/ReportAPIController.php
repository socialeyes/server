<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\API\BaseAPIController;
use App\Models\AuthorizeReport;
use App\Models\Report;
use App\Models\ReportDetail;
use App\Models\Reporter;
use App\Models\ReportStatus;
use App\Models\ReportType;
use App\Repositories\AuthorizeReportRepository;
use App\Repositories\ReportDetailRepository;
use App\Repositories\ReporterRepository;
use App\Repositories\ReportRepository;
use App\Repositories\ReportStatusRepository;
use App\Repositories\ReportTypeRepository;
use App\Utilities\FileUtility;
use App\Utilities\ResponseUtility;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Response;
use Validator;

class ReportAPIController extends BaseAPIController
{
    /** @var ReportRepository */
    private $reportRepository;

    /** @var ReportStatusRepository */
    private $reportStatusRepository;

    /** @var ReporterRepository */
    private $reporterRepository;

    /** @var AuthorizeReportRepository */
    private $authorizeReportRepository;

    /** @var ReportTypeRepository */
    private $reportTypeRepository;

    /** @var ReportDetailRepository */
    private $reportDetailRepository;

    /**
     * @param ReportRepository $reportRepository
     * @param ReportStatusRepository $reportStatusRepository
     * @param ReporterRepository $reporterRepository
     * @param AuthorizeReportRepository $authorizeReportRepository
     * @param ReportTypeRepository $reportTypeRepository
     * @param ReportDetailRepository $reportDetailRepository
     */
    public function __construct(
        ReportRepository $reportRepository,
        ReportStatusRepository $reportStatusRepository,
        ReporterRepository $reporterRepository,
        AuthorizeReportRepository $authorizeReportRepository,
        ReportTypeRepository $reportTypeRepository,
        ReportDetailRepository $reportDetailRepository
    ) {
        parent::__construct();
        $this->reportRepository = $reportRepository;
        $this->reportStatusRepository = $reportStatusRepository;
        $this->reporterRepository = $reporterRepository;
        $this->authorizeReportRepository = $authorizeReportRepository;
        $this->reportTypeRepository = $reportTypeRepository;
        $this->reportDetailRepository = $reportDetailRepository;
    }

    /**
     * @param int $reporterId
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/reports/{reporterId}",
     *      summary="Get all reports by reporter",
     *      tags={"Report"},
     *      description="Provide `access_token` in Header request",
     *      produces={"application/json"},
     *
     *      @SWG\Parameter(
     *          name="access_token",
     *          description="API token of server",
     *          type="string",
     *          required=true,
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="limit",
     *          in="formData",
     *          description="Limit number of reports per request",
     *          required=false,
     *          type="string"
     *      ),
     *     @SWG\Parameter(
     *      name="page[number]",
     *      description="The page number of the pagination. Ex: page[number]=1, page[number]=2",
     *      type="string",
     *      required=false,
     *      in="formData"
     *    ),
     *
     *     @SWG\Response(
     *          response=200,
     *          description="Report list",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Report")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=400,
     *          description="Reporter is required."
     *      ),
     *      @SWG\Response(
     *          response=404,
     *          description="Reporter was not found."
     *      )
     * )
     */
    public function index($reporterId, Request $request)
    {
        /** @var Reporter|null $reporter */
        $reporter = $this->reporterRepository->findWithoutFail($reporterId);
        if (empty($reporter)) {
            return ResponseUtility::sendError('Reporter does not found.', 422);
        }

        $limit = config('json-api-paginate.max_results');
        if ($request->has('limit')) {
            $limit = intval($request->get('limit'));
        }

        $reports = Report::orderBy('updated_at', 'DESC')
            ->where('reporter_id', '=', $reporterId);

        $numPage = ($reports->count() < $limit) ? 1 : floor($reports->count() / $limit);
        $responseData = [
            'reports' => Report::getResponseData($reports->jsonPaginate($limit)),
            'numPaginate' => $numPage
        ];
        return ResponseUtility::sendResponse($responseData, 'Reports list');
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/reports",
     *      summary="Store new report",
     *      tags={"Report"},
     *      description="Provide `access_token` in Header request",
     *      produces={"application/json"},
     *
     *      @SWG\Parameter(
     *          name="access_token",
     *          description="API token of server",
     *          type="string",
     *          required=true,
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="reporter_id",
     *          in="formData",
     *          description="Registered reporter id",
     *          required=true,
     *          type="integer"
     *      ),
     *      @SWG\Parameter(
     *          name="report_type_id",
     *          in="formData",
     *          description="Report type id",
     *          required=true,
     *          type="integer"
     *      ),
     *      @SWG\Parameter(
     *          name="authorize_reports",
     *          in="formData",
     *          description="Separate by camma. Ex: 1,2,3",
     *          required=true,
     *          type="string"
     *
     *      ),
     *      @SWG\Parameter(
     *          name="audio",
     *          description="Reporting audio. Audio file path from the pre-upload request",
     *          type="string",
     *          required=true,
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="image",
     *          description="Reporting image. Image file path from the pre-upload request",
     *          type="string",
     *          required=true,
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="description",
     *          description="Reporting description. Provide empty string if nothing written",
     *          type="string",
     *          required=true,
     *          in="formData"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="Report was retrieved successfully.",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Report")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=400,
     *          description="Fields are required."
     *      ),
     *      @SWG\Response(
     *          response=404,
     *          description="Reporter was not found."
     *      ),
     *     @SWG\Response(
     *          response=422,
     *          description="There is no default report status. OR Report does not found."
     *      )
     * )
     */
    public function store(Request $request)
    {
        /** @var ReportStatus|null $reportStatus */
        $reportStatus = $this->reportStatusRepository->findWhere(['name' => 'Reporting'])->first();
        if (empty($reportStatus)) {
            return ResponseUtility::sendError('There is no default report status.', 422);
        }


        $input = $request->all();
        $input['report_status_id'] = $reportStatus->id;
        if (!$request->has('reporter_id') || !$request->has('report_type_id') || !$request->has('authorize_reports') || !$request->has('audio') || !$request->has('image') || !$request->has('description')) {
            return ResponseUtility::sendError('Fields are required.', 400);
        }

        /** @var Reporter|null $reporter */
        $reporter = $this->reporterRepository->findWithoutFail($input['reporter_id']);
        if (empty($reportStatus)) {
            return ResponseUtility::sendError('There is no reporter.', 422);
        }

        if (trim($input['audio']) !== '') {
            $path = str_replace('public/', 'storage/', Report::AUDI0_STORAGE_PATH);
            $input['audio'] = FileUtility::saveUploadResource($input['audio'], $path);
        }

        if (trim($input['image']) !== '') {
            $path = str_replace('public/', 'storage/', Report::IMAGE_STORAGE_PATH);
            $input['image'] = FileUtility::saveUploadResource($input['image'], $path);
        }

        $reportData = [
            'reporter_id' => $input['reporter_id'],
            'report_status_id' => $reportStatus->id,
            'report_type_id' => $input['report_type_id'],
            'audio' => $input['audio'],
            'image' => $input['image'],
            'description' => $input['description']
        ];
        /** @var Report $report */
        $report = $this->reportRepository->create($reportData);

        $arrAuthorizeReports = explode(',', $input['authorize_reports']);
        foreach($arrAuthorizeReports as $authorizeReportId) {
            $storeAuthorizeReportId = intval($authorizeReportId);
            /** @var AuthorizeReport $authorizeReport */
            $authorizeReport = $this->authorizeReportRepository->findWithoutFail($storeAuthorizeReportId);
            if (!empty($authorizeReport)) {
                $reportDetailData = [
                    'report_id' => $report->id,
                    'authorize_report_id' => $storeAuthorizeReportId,
                    'report_status_id' => $reportStatus->id
                ];
                /** @var ReportDetail $reportDetail */
                $reportDetail = $this->reportDetailRepository->create($reportDetailData);

                // Send SMS
                $contactPhone =  $authorizeReport->authorize_place->contact_phone;
                if ($contactPhone) {
                    $from = config('app.name');
                    $to = $contactPhone;
                    $message = 'Reported from ' . $reporter->getDisplayName() . '. Detail at ' . env('APP_URL') . '/reports/' . $report->id . '/view .';
                    $this->sendSMS($from, $to, $message);
                }
            }
        }

        $response = $report->getSingleResponseData();
        return ResponseUtility::sendResponse($response, 'Report was retrieved successfully.');
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Delete(
     *      path="/reports",
     *      summary="Delete report by reporter",
     *      tags={"Report"},
     *      description="Delete report by reporter",
     *      produces={"application/json"},
     *
     *      @SWG\Parameter(
     *          name="access_token",
     *          description="Access token",
     *          type="string",
     *          required=true,
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="reporter_id",
     *          in="formData",
     *          description="Reporter id",
     *          required=true,
     *          type="integer"
     *      ),
     *      @SWG\Parameter(
     *          name="report_id",
     *          description="Report id",
     *          type="integer",
     *          required=true,
     *          in="formData"
     *      ),
     *      @SWG\Parameter(
     *          name="delete_reason",
     *          description="Tell why you want to delete this report",
     *          type="string",
     *          required=true,
     *          in="formData"
     *      ),
     *
     *     @SWG\Response(
     *          response=200,
     *          description="Report was deleted successfully",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(
     *                      @SWG\Property(property="reporter_id", type="integer"),
     *                      @SWG\Property(property="report_id", type="integer")
     *                  )
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=400,
     *          description="Fields are required."
     *      ),
     *      @SWG\Response(
     *          response=404,
     *          description="Reporter was not found."
     *      )
     * )
     */
    public function delete(Request $request)
    {
        if (!$request->has('reporter_id') || !$request->has('report_id') || !$request->has('delete_reason')) {
            return ResponseUtility::sendError('Fields are required.', 400);
        }

        $reportId = intval($request->get('report_id'));
        $reporterId = intval($request->get('reporter_id'));
        $report = $this->reportRepository->findWhere([
            'id' => $reportId,
            'reporter_id' => $reporterId
        ])->first();

        if (empty($report)) {
            return ResponseUtility::sendError('Report was not found.', 404);
        }

        // Save delete reason
        $updateData = [
            'delete_reason' => $request->get('delete_reason')
        ];
        $report = $this->reportRepository->update($updateData, $reportId);

        // Soft delete report
        $this->reportRepository->delete($report->id);

        $responseData = [
            'reporter_id' => $reporterId,
            'report_id' => $reportId
        ];
        return ResponseUtility::sendResponse($responseData, 'Report was deleted successfully.');
    }

    /**
     * @param string $from
     * @param string $to
     * @param string $message
     */
    protected function sendSMS($from, $to, $message)
    {
        $basic  = new \Nexmo\Client\Credentials\Basic(config('nexmo.api_key'), config('nexmo.api_secret'));
        $client = new \Nexmo\Client($basic);
        $message = $client->message()->send([
            'to' => $to,
            'from' => $from,
            'text' => $message
        ]);
    }
}
