<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\API\BaseAPIController;
use App\Models\ReportType;
use App\Repositories\ReportTypeRepository;
use App\Utilities\ResponseUtility;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ReportTypeAPIController extends BaseAPIController
{
    /** @var  ReportTypeRepository */
    private $reportTypeRepository;

    /**
     * @param ReportTypeRepository $reportTypeRepository
     */
    public function __construct(ReportTypeRepository $reportTypeRepository)
    {
        parent::__construct();
        $this->reportTypeRepository = $reportTypeRepository;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/report_types",
     *      summary="Get a listing of the report type.",
     *      tags={"ReportType"},
     *      description="Provide `api_token` in Header request",
     *      produces={"application/json"},
     *
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/ReportType")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->reportTypeRepository->pushCriteria(new RequestCriteria($request));
        $this->reportTypeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $reportTypes = $this->reportTypeRepository->all();

        return ResponseUtility::sendResponse(ReportType::getAllResponseData($reportTypes), 'ReportType retrieved successfully');
    }
}
