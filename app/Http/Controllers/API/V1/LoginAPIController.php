<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\API\BaseAPIController;
use App\Models\Reporter;
use App\Repositories\ReporterRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Utilities\ResponseUtility;
use Response;
use Validator;

class LoginAPIController extends BaseAPIController
{
    /** @var ReporterRepository */
    private $reporterRepository;

    /**
     * @param ReporterRepository $reporterRepository
     */
    public function __construct(ReporterRepository $reporterRepository)
    {
        parent::__construct();
        $this->reporterRepository = $reporterRepository;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/authorization/login",
     *      summary="Login",
     *      tags={"Authorization"},
     *      description="Login user into system",
     *      produces={"application/json"},
     *
     *
     *      @SWG\Parameter(
     *          name="api_token",
     *          description="api token of server",
     *          type="string",
     *          required=true,
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="email",
     *          in="formData",
     *          description="Reporter email",
     *          required=true,
     *          type="string"
     *      ),
     *      @SWG\Parameter(
     *          name="password",
     *          in="formData",
     *          description="Reporter password",
     *          required=true,
     *          type="string"
     *      ),
     *
     *      @SWG\Response(
     *          response=200,
     *          description="Logged in successfully",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Reporter")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=400,
     *          description="Fields are required."
     *      ),
     *      @SWG\Response(
     *          response=401,
     *          description="Password is incorrect."
     *      ),
     *      @SWG\Response(
     *          response=404,
     *          description="Reporter was not found."
     *      )
     * )
     */
    public function login(Request $request)
    {
        if (!$request->has('email') || !$request->has('password')) {
            return ResponseUtility::sendError('Fields are required.', 400);
        }

        $input = $request->all();
        $input['email'] = trim($input['email']);

        /** @var Reporter|null $reporter */
        $reporter = $this->reporterRepository->findWhere([
            ['email', '=', $input['email']],
            ['activate', '=', 1],
        ])->first();

        if (empty($reporter)) {
            return ResponseUtility::sendError('Reporter was not found.', 404);
        }

        $credentials = [
            'email' => $input['email'],
            'password' => $input['password']
        ];
        if ($this->guard()->attempt($credentials)) {
            $updateData = [
                'remember_token' => Reporter::generateAccessToken($input['email']),
                'started_session' => Carbon::now()->toDateTimeString()

            ];
            $reporter = $this->reporterRepository->update($updateData, $reporter->id);
            $responseData = $reporter->getSingleResponseData();
            return ResponseUtility::sendResponse($responseData, 'Logged in successfully');
        }

        return ResponseUtility::sendError('Password is incorrect.', 401);
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/authorization/logout",
     *      summary="Logout",
     *      tags={"Authorization"},
     *      description="Logout user into system",
     *      produces={"application/json"},
     *
     *
     *      @SWG\Parameter(
     *          name="access_token",
     *          description="access token of server",
     *          type="string",
     *          required=true,
     *          in="header"
     *      ),
     *
     *      @SWG\Response(
     *          response=200,
     *          description="Logout successfully",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Reporter")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=400,
     *          description="Fields are required."
     *      ),
     *      @SWG\Response(
     *          response=404,
     *          description="Reporter was not found."
     *      )
     * )
     */
    public function logout(Request $request)
    {
        $accessToken = $this->getAccessTokenFromRequest($request);
        /** @var Reporter|null $reporter */
        $reporter = $this->reporterRepository->findWhere([
            ['remember_token', '=', $accessToken],
            ['activate', '=', 1],
        ])->first();
        if (empty($reporter)) {
            return ResponseUtility::sendError('Reporter was not found.', 404);
        }

        $updateData = [
            'remember_token' => null

        ];
        $reporter = $this->reporterRepository->update($updateData, $reporter->id);
        return ResponseUtility::sendResponse([], 'Logout successfully');
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/authorization/check_user_session",
     *      summary="Check whether user already logged in",
     *      tags={"Authorization"},
     *      description="Check whether user already logged in",
     *      produces={"application/json"},
     *
     *
     *      @SWG\Parameter(
     *          name="access_token",
     *          description="access token of server",
     *          type="string",
     *          required=true,
     *          in="header"
     *      ),
     *
     *      @SWG\Response(
     *          response=200,
     *          description="Already logged in.",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Reporter")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=400,
     *          description="Fields are required."
     *      ),
     *      @SWG\Response(
     *          response=422,
     *          description="Not yet log in."
     *      )
     * )
     */
    public function checkUserSession(Request $request)
    {
        $accessToken = $this->getAccessTokenFromRequest($request);
        /** @var Reporter|null $reporter */
        $reporter = $this->reporterRepository->findWhere([
            ['remember_token', '=', $accessToken],
            ['activate', '=', 1],
        ])->first();
        if (empty($reporter)) {
            return ResponseUtility::sendError('Not yet log in.', 422);
        }

        return ResponseUtility::sendResponse([], 'Already logged in.');
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/authorization/send_reset_password_code",
     *      summary="Send reset password code to email",
     *      tags={"Authorization"},
     *      description="Send reset password code to email",
     *      produces={"application/json"},
     *
     *
     *      @SWG\Parameter(
     *          name="api_token",
     *          description="api token of server",
     *          type="string",
     *          required=true,
     *          in="header"
     *      ),
     *
     *      @SWG\Parameter(
     *          name="email",
     *          in="formData",
     *          description="Reporter email",
     *          required=true,
     *          type="string"
     *      ),
     *
     *      @SWG\Response(
     *          response=200,
     *          description="Reset password was sent to your email.",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Reporter")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=400,
     *          description="Fields are required."
     *      ),
     *      @SWG\Response(
     *          response=422,
     *          description="Email does not exist."
     *      )
     * )
     */
    public function sendResetPasswordCode(Request $request)
    {
        if (!$request->has('email')) {
            return ResponseUtility::sendError('Fields are required.', 400);
        }

        $email = trim($request->get('email'));

        /** @var Reporter|null $reporter */
        $reporter = $this->reporterRepository->findWhere([
            ['email', '=', $email],
            ['activate', '=', 1],
        ])->first();
        if (empty($reporter)) {
            return ResponseUtility::sendError('Email does not exist.', 422);
        }

        /**
         * Send reset password code
         */
        $forgotPasswordConfiguration =  config('setting.activation.forgot_password');
        $verifyCode = Reporter::generateVerifyCode(intval($forgotPasswordConfiguration['code_length']));
        $forgotPasswordConfiguration['name'] = $reporter->getDisplayName();
        $forgotPasswordConfiguration['email'] = $reporter->email;
        $forgotPasswordConfiguration['verify_code'] = $verifyCode;
        Reporter::sendVerificationCodeToEmail($forgotPasswordConfiguration);

        $updateData = [
            'remember_token' => $verifyCode,
            'started_session' => null

        ];
        $reporter = $this->reporterRepository->update($updateData, $reporter->id);

        $responseData = [
            'id' => $reporter->id,
            'email' => $reporter->email
        ];
        return ResponseUtility::sendResponse($responseData, 'Reset password was sent to your email.');
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/authorization/reset_password",
     *      summary="Reset password",
     *      tags={"Authorization"},
     *      description="Reset password",
     *      produces={"application/json"},
     *
     *
     *      @SWG\Parameter(
     *          name="api_token",
     *          description="api token of server",
     *          type="string",
     *          required=true,
     *          in="header"
     *      ),
     *
     *      @SWG\Parameter(
     *          name="email",
     *          in="formData",
     *          description="Reporter email",
     *          required=true,
     *          type="string"
     *      ),
     *     @SWG\Parameter(
     *          name="verify_code",
     *          in="formData",
     *          description="Verify code",
     *          required=true,
     *          type="string"
     *      ),
     *     @SWG\Parameter(
     *          name="password",
     *          in="formData",
     *          description="new password",
     *          required=true,
     *          type="string"
     *      ),
     *
     *      @SWG\Response(
     *          response=200,
     *          description="Reset password successfully",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Reporter")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      ),
     *      @SWG\Response(
     *          response=400,
     *          description="Fields are required."
     *      ),
     *      @SWG\Response(
     *          response=422,
     *          description="Reporter does not exist."
     *      )
     * )
     */
    public function resetPassword(Request $request)
    {
        if (!$request->has('email') || !$request->has('verify_code') || !$request->has('password')) {
            return ResponseUtility::sendError('Fields are required.', 400);
        }

        $email = trim($request->get('email'));
        $verifyCode = trim($request->get('verify_code'));

        /** @var Reporter|null $reporter */
        $reporter = $this->reporterRepository->findWhere([
            ['email', '=', $email],
            ['remember_token', '=', $verifyCode],
            ['activate', '=', 1],
        ])->first();
        if (empty($reporter)) {
            return ResponseUtility::sendError('Reporter does not exist.', 422);
        }

        $updateData = [
            'password' => bcrypt($request->get('password')),
            'remember_token' => null,
            'started_session' => null

        ];
        $reporter = $this->reporterRepository->update($updateData, $reporter->id);

        return ResponseUtility::sendResponse($reporter->getSingleResponseData(), 'Reset password successfully');
    }
}