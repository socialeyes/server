<?php
namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\API\BaseAPIController;
use App\Models\Report;
use App\Utilities\FileUtility;
use App\Utilities\ResponseUtility;
use Illuminate\Http\Request;
use Response;

/**
 * @package App\Http\Controllers\API
 */
class FileAPIController extends BaseAPIController
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/file_upload",
     *      summary="A pre-upload a file (audio|image|video) to server",
     *      tags={"File"},
     *      description="A pre-upload a file (audio|image|video) to server",
     *      produces={"application/json"},
     *
     *    @SWG\Parameter(
     *      name="api_token",
     *      description="Provide `api_token` in Header request.",
     *      type="string",
     *      required=false,
     *      in="header"
     *    ),
     *     @SWG\Parameter(
     *      name="file",
     *      description="File to be upload.",
     *      type="file",
     *      required=true,
     *      in="formData"
     *   ),
     *
     *    @SWG\Response(
     *        response=200,
     *        description="A file was uploaded successfully.",
     *        @SWG\Schema(
     *           type="object",
     *           @SWG\Property(
     *             property="success",
     *             type="boolean"
     *           ),
     *           @SWG\Property(
     *               property="version",
     *               type="string"
     *           ),
     *           @SWG\Property(
     *             property="data",
     *             type="string"
     *           ),
     *          @SWG\Property(
     *             property="message",
     *             type="string"
     *          )
     *        )
     *    ),
     *    @SWG\Response(
     *       response=400,
     *       description="Field `file` is required."
     *    )
     * )
     */
    public function upload(Request $request)
    {
        if (!$request->hasFile('file')) {
            return ResponseUtility::sendError('Field `file` is required.', 400);
        }

        $path = $request->file('file')->storePublicly('public/temp');
        $pathAndFileName = str_replace('public/', 'storage/', $path);
        return ResponseUtility::sendResponse(['file' => asset($pathAndFileName)], 'A file was uploaded successfully.');
    }
}