<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use Flash;
use Response;

class HomeController extends FrontendBaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->activePage = ['main' => '/', 'sub' => ''];
    }

	/**
	 * @return Response
	 */
	public function index()
	{
		return $this->assignToView('welcome', 'Home');
	}
}
