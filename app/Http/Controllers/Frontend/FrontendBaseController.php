<?php
namespace App\Http\Controllers\Frontend;

/**
 *  This class is belong to SportPill
 */

use App\Http\Controllers\Controller;

class FrontendBaseController extends Controller
{

    public function __construct()
	{
        $this->middleware('guest');
        $this->viewPath = 'frontends.';
    }

	/**
	 * @param $viewAction
     * @param $pageTitle
	 * @param array $values
	 *
	 * @return mixed
	 */
	public function assignToView($viewAction, $pageTitle = '', $values = array())
	{
		$view = view($this->viewPath . $viewAction)
			->with('pageTitle', $pageTitle)
			->with('activeMenu', $this->activeMenu);

		foreach ($values as $key => $value) {
			$view = $view->with($key, $value);
		}

		return $view;
	}
}
