<?php

namespace App\Http\Controllers\Frontend;

use App\Repositories\ReportRepository;
use Flash;
use Response;

class ReportController extends FrontendBaseController
{

    /** @var ReportRepository */
    private $reportRepository;

	public function __construct(ReportRepository $reportRepository)
	{
		parent::__construct();
        $this->viewPath = 'frontends.reports.';
        $this->routePath = 'reports.';
        $this->reportRepository = $reportRepository;
    }

	/**
     * @param int id
	 * @return Response
	 */
	public function show($id)
	{
	    $report = $this->reportRepository->findWithoutFail($id);
	    if (empty($report)) {
            return redirect(route('homepage'));
        }
		return $this->assignToView('show', 'Report detail', [
		    'report' => $report
        ]);
	}
}
