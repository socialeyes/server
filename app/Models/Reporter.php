<?php

namespace App\Models;

use App\Mail\SendActivatedCode;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

/**
 * @SWG\Definition(
 *      definition="Reporter",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="first_name",
 *          description="first_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="last_name",
 *          description="last_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="sex",
 *          description="sex",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="birth_date",
 *          description="birth_date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="address",
 *          description="address",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="activate",
 *          description="activate",
 *          type="int32"
 *      ),
 *      @SWG\Property(
 *          property="last_logged_in",
 *          description="last_logged_in",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="image",
 *          description="image",
 *          type="string"
 *      )
 *
 * )
 */
class Reporter extends Authenticatable
{
    use SoftDeletes;

    const IMAGE_STORAGE_PATH = 'public/reporter/images';

    public $table = 'reporters';
    protected $dates = ['deleted_at'];

    public $fillable = [
        'remember_token',
        'email',
        'password',
        'first_name',
        'last_name',
        'sex',
        'birth_date',
        'address',
        'image',
        'started_session',
        'activate'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'email' => 'required|email',
        'password' => 'required|min:8',
        'sex' => 'required',
        'address' => 'required',
        'birth_date' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reports()
    {
        return $this->hasMany('App\Models\Report');
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        $fullName = trim($this->getFullName());
        if ($fullName === '') {
            $fullName = 'Sir / Madam';
        }
        return $fullName;
    }

    /**
     * @param array $configuration
     */
    public static function sendVerificationCodeToEmail($configuration)
    {
        Mail::to($configuration['email'])->send(new SendActivatedCode($configuration));
    }

    /**
     * @param int $length
     * @return string
     */
    public static function generateVerifyCode($length)
    {
        $intMin = (10 ** $length) / 10;
        $intMax = (10 ** $length) - 1;
        $codeRandom = mt_rand($intMin, $intMax);

        return (string) $codeRandom;
    }

    /**
     * @param $string
     * @return string
     */
    public static function generateAccessToken($string)
    {
        return bcrypt($string . time());
    }

    /**
     * @param Reporter $reporter
     * @param int $expireTime : in second
     *
     * @return bool
     */
    public static function isVerificationPeriodValid($reporter, $expireTime = 3600)
    {
        $isValid = false;
        $lastLoginTimestamp = strtotime($reporter->updated_at);
        $time = time();
        if ($time - $lastLoginTimestamp < $expireTime) {
            $isValid = true;
        }

        return $isValid;
    }

    /**
     * @return bool
     */
    public function isSessionValid()
    {
        $isValid = false;
        $expireTime = 60 * intval(config('auth.passwords.reporters.expire'));
        $lastLoginTimestamp = strtotime($this->started_session);
        $time = time();
        if ($time - $lastLoginTimestamp < $expireTime) {
            $isValid = true;
        }

        return $isValid;
    }

    /**
     * @return array
     */
    public function getSingleResponseData()
    {
        return [
            'id' => $this->id,
            'email' => $this->email,
            'first_name' => $this->first_name . '',
            'last_name' => $this->last_name . '',
            'sex' => $this->sex,
            'birth_date' => $this->birth_date,
            'address' => $this->address,
            'image' => ($this->image) ? asset($this->image) : '',
            'activate' => $this->activate,
            'started_session' => $this->started_session,
            'remember_token' => $this->remember_token
        ];
    }
}
