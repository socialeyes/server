<?php

namespace App\Models;

use Eloquent as Model;

/**
 * @SWG\Definition(
 *      definition="ReportType",
 *      required={"name", "image"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="image",
 *          description="image",
 *          type="string"
 *      )
 * )
 */
class ReportType extends Model
{

    public $table = 'report_types';
    public $timestamps = false;

    public $fillable = [
        'name',
        'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|min:3',
        'image' => 'required'
    ];

    /**
     * @return array
     */
    public function getSingleResponseData()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => asset($this->image)
        ];
    }

    /**
     * @return array
     */
    public static function getAllResponseData($reportTypes)
    {
        $response = [];
        /** @var ReportType $reportType */
        foreach ($reportTypes as $reportType) {
            $response[] = $reportType->getSingleResponseData();
        }
        return $response;
    }
}
