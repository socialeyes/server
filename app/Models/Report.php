<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Report",
 *      required={"reporter_id", "report_status_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="reporter",
 *          description="reporter",
 *          ref="#/definitions/Reporter"
 *      ),
 *      @SWG\Property(
 *          property="report_status",
 *          description="report_status",
 *          ref="#/definitions/ReportStatus"
 *      ),
 *      @SWG\Property(
 *          property="report_type",
 *          description="report_type",
 *          ref="#/definitions/ReportType"
 *      ),
 *      @SWG\Property(
 *          property="audio",
 *          description="audio",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="image",
 *          description="image",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Report extends Model
{
    use SoftDeletes;

    public $table = 'reports';
    protected $dates = ['deleted_at'];

    const AUDI0_STORAGE_PATH = 'public/report/audios';
    const IMAGE_STORAGE_PATH = 'public/report/images';

    public $fillable = [
        'reporter_id',
        'report_status_id',
        'report_type_id',
        'audio',
        'image',
        'description',
        'delete_reason'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'reporter_id' => 'required',
        'report_status_id' => 'required',
        'report_type_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function report_status()
    {
        return $this->belongsTo('App\Models\ReportStatus');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function report_type()
    {
        return $this->belongsTo('App\Models\ReportType');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reporter()
    {
        return $this->belongsTo('App\Models\Reporter');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function report_details()
    {
        return $this->hasMany('App\Models\ReportDetail');
    }

    /**
     * @return array
     */
    public function getSingleResponseData()
    {
        /** @var ReportStatus $reportStatus */
        $reportStatus = $this->report_status;

        /** @var ReportType $reportType */
        $reportType = $this->report_type;

        /** @var Reporter $reporter */
        $reporter = $this->reporter;

        $reportDetails = [];
        /** @var ReportDetail $reportDetail */
        foreach ($this->report_details as $reportDetail) {
            $reportDetails[] = $reportDetail->getSingleResponseData();
        }

        return [
            'id' => $this->id,
            'report_status' => $reportStatus->getSingleResponseData(),
            'report_type' => $reportType->getSingleResponseData(),
            'reporter' => $reporter->getSingleResponseData(),
            'audio' => $this->audio ? asset($this->audio) : '',
            'image' => $this->image ? asset($this->image) : '',
            'description' => $this->description,
            'created_at' => $this->created_at,
            'report_details' => $reportDetails
        ];
    }

    /**
     * @param $reports
     * @return array
     */
    public static function getResponseData($reports)
    {
        $data = [];
        /** @var Report $report */
        foreach ($reports as $report) {
            $data[] = $report->getSingleResponseData();
        }
        return $data;
    }

    /**
     * @return int
     */
    public function getReportStatusIdToUpdate()
    {
        $reportStatusId = 0;
        /** @var ReportDetail $reportDetail */
        foreach ($this->report_details as $reportDetail) {
            if ($reportDetail->report_status->id > $reportStatusId) {
                $reportStatusId = $reportDetail->report_status->id;
            }
        }

        return $reportStatusId;
    }
}
