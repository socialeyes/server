<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="AuthorizePlace",
 *      required={"authorize_type_id", "name", "email", "contact_phone"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="authorize_type",
 *          description="authorize_type",
 *          ref="#/definitions/AuthorizeType"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="logo",
 *          description="logo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="contact_phone",
 *          description="contact_phone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="latitude",
 *          description="latitude",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="longitude",
 *          description="longitude",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class AuthorizePlace extends Model
{
    use SoftDeletes;

    public $table = 'authorize_places';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    protected $dates = ['deleted_at'];


    public $fillable = [
        'authorize_type_id',
        'name',
        'email',
        'logo',
        'contact_phone',
        'latitude',
        'longitude',
        'active'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'authorize_type_id' => 'required',
        'name' => 'required|min:3',
        'email' => 'required',
        'logo' => 'contact_phone string text',
        'contact_phone' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\Be
     */
    public function authorize_type()
    {
        return $this->belongsTo('App\Models\AuthorizeType');
    }

    /**
     * @return array
     */
    public function getSingleResponseData()
    {
        return [
            'id' => $this->id,
            'authorize_type_id' => $this->authorize_type_id,
            'name' => $this->name,
            'email' => $this->email,
            'contact_phone' => $this->contact_phone,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'logo' => asset($this->logo)
        ];
    }

    /**
     * @return array
     */
    public static function getAllResponseData($reportTypes)
    {
        $response = [];
        /** @var ReportType $reportType */
        foreach ($reportTypes as $reportType) {
            $response[] = $reportType->getSingleResponseData();
        }
        return $response;
    }
}
