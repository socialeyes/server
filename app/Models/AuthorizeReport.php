<?php

namespace App\Models;

use Eloquent as Model;


/**
 * @SWG\Definition(
 *      definition="AuthorizeReport",
 *      required={"authorize_place_id", "report_type_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="authorize_place",
 *          description="authorize_place",
 *          ref="#/definitions/AuthorizePlace"
 *      ),
 *      @SWG\Property(
 *          property="report_type",
 *          description="report_type",
 *          ref="#/definitions/AuthorizeType"
 *      )
 * )
 */
class AuthorizeReport extends Model {

    public $table = 'authorize_reports';
    public $timestamps = false;

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    public $fillable = [
        'authorize_place_id',
        'report_type_id'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'authorize_place_id' => 'required',
        'report_type_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function authorize_place()
    {
        return $this->belongsTo('App\Models\AuthorizePlace');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function report_type()
    {
        return $this->belongsTo('App\Models\ReportType');
    }

    /**
     * @return array
     */
    public function getSingleResponseData()
    {
        $data = [
            'id' => $this->id
        ];

        /** @var AuthorizePlace|null $authorizePlace */
        $authorizePlace = $this->authorize_place;
        if ($authorizePlace) {
            $data['authorize_place'] = $authorizePlace->getSingleResponseData();
        }

        /** @var ReportType|null $reportType */
        $reportType = $this->report_type;
        if ($reportType) {
            $data['report_type'] = $reportType->getSingleResponseData();
        }

        return $data;
    }

    /**
     * @param $authorizeReports
     * @return array
     */
    public static function getResponseData($authorizeReports)
    {
        $data = [];
        /** @var AuthorizeReport $authorizeReport */
        foreach ($authorizeReports as $authorizeReport) {
            $data[] = $authorizeReport->getSingleResponseData();
        }
        return $data;
    }
}