<?php

namespace App\Models;

use Eloquent as Model;

class ReportDetail extends Model{

    public $table = 'report_details';
    public $timestamps = false;

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
    ];

    public $fillable = [
        'report_id',
        'authorize_report_id',
        'report_status_id',
        'response_datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'authorize_report_id' => 'required',
        'report_id' => 'required',
        'report_status_id' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function report_status()
    {
        return $this->belongsTo('App\Models\ReportStatus');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function report()
    {
        return $this->belongsTo('App\Models\Report');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function authorize_report()
    {
        return $this->belongsTo('App\Models\AuthorizeReport');
    }

    public function getSingleResponseData() {
        /** @var ReportStatus $reportStatus */
        $reportStatus = $this->report_status;

        /** @var AuthorizeReport $authorizeReport */
        $authorizeReport = $this->authorize_report;

        return [
            'id' => $this->id,
            'report_status' => $reportStatus->getSingleResponseData(),
            'authorize_report' => $authorizeReport->getSingleResponseData(),
            'response_datetime' => $this->response_datetime
        ];
    }
}