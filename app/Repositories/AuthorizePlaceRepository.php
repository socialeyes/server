<?php

namespace App\Repositories;

use App\Models\AuthorizePlace;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AuthorizePlaceRepository
 * @package App\Repositories
 * @version May 15, 2018, 9:50 pm UTC
 *
 * @method AuthorizePlace findWithoutFail($id, $columns = ['*'])
 * @method AuthorizePlace find($id, $columns = ['*'])
 * @method AuthorizePlace first($columns = ['*'])
*/
class AuthorizePlaceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'authorize_type_id',
        'name',
        'email',
        'logo',
        'contact_phone',
        'latitude',
        'longitude',
        'active'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AuthorizePlace::class;
    }
}
