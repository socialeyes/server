<?php

namespace App\Repositories;

use App\Models\AuthorizeReport;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AuthorizeReportRepository
 * @package App\Repositories
 * @version May 15, 2018, 9:50 pm UTC
 *
 * @method AuthorizeReport findWithoutFail($id, $columns = ['*'])
 * @method AuthorizeReport find($id, $columns = ['*'])
 * @method AuthorizeReport first($columns = ['*'])
*/
class AuthorizeReportRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'authorize_type_id',
        'name',
        'email',
        'logo',
        'contact_phone',
        'latitude',
        'longitude',
        'active'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AuthorizeReport::class;
    }
}
