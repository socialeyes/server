<?php

namespace App\Repositories;

use App\Models\ReportType;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ReportTypeRepository
 * @package App\Repositories
 * @version May 15, 2018, 9:38 pm UTC
 *
 * @method ReportType findWithoutFail($id, $columns = ['*'])
 * @method ReportType find($id, $columns = ['*'])
 * @method ReportType first($columns = ['*'])
*/
class ReportTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ReportType::class;
    }
}
