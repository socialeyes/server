<?php

namespace App\Repositories;

use App\Models\Report;
use App\Models\ReportDetail;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ReportDetailRepository
 * @package App\Repositories
 * @version June 7, 2018, 7:43 pm UTC
 *
 * @method ReportDetail findWithoutFail($id, $columns = ['*'])
 * @method ReportDetail find($id, $columns = ['*'])
 * @method ReportDetail first($columns = ['*'])
*/
class ReportDetailRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'reporter_id',
        'audio',
        'image',
        'description',
        'report_status_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ReportDetail::class;
    }
}
