<?php

namespace App\Repositories;

use App\Models\ReportStatus;
use InfyOm\Generator\Common\BaseRepository;

/**
 * @method ReportStatus findWithoutFail($id, $columns = ['*'])
 * @method ReportStatus find($id, $columns = ['*'])
 * @method ReportStatus first($columns = ['*'])
*/
class ReportStatusRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ReportStatus::class;
    }
}
