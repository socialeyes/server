<?php

namespace App\Repositories;

use App\Models\AuthorizeType;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AuthorizeTypeRepository
 * @package App\Repositories
 * @version May 15, 2018, 9:38 pm UTC
 *
 * @method AuthorizeType findWithoutFail($id, $columns = ['*'])
 * @method AuthorizeType find($id, $columns = ['*'])
 * @method AuthorizeType first($columns = ['*'])
*/
class AuthorizeTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AuthorizeType::class;
    }
}
