<?php

namespace App\Repositories;

use App\Models\Reporter;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ReporterRepository
 * @package App\Repositories
 * @version May 15, 2018, 8:08 pm UTC
 *
 * @method Reporter findWithoutFail($id, $columns = ['*'])
 * @method Reporter find($id, $columns = ['*'])
 * @method Reporter first($columns = ['*'])
*/
class ReporterRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'email',
        'password',
        'first_name',
        'last_name',
        'nick_name',
        'sex',
        'birth_date',
        'address',
        'image',
        'last_logged_in'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Reporter::class;
    }
}
