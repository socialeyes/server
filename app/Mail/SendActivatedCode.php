<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendActivatedCode extends Mailable
{
    use Queueable, SerializesModels;

    private $content = [];

    /**
     * @param array $content
     */
    public function __construct($content = [])
    {
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(config('setting.activation.sender_address'), config('setting.activation.sender_name'))
            ->subject($this->content['subject'])
            ->markdown($this->content['template'])
            ->with('content', $this->content);
    }
}
