<?php

return [
    'label' => 'Report',
    'reporter' => 'Reporter',
    'status' => 'Status',
    'report_type' => 'Report type',
    'audio' => 'Audio',
    'image' => 'Image',
    'description' => 'Description',
    'created_at' => 'Created at',
    'report_detail' => 'Report Details',
    'authorize_report' => 'Reported to'
];