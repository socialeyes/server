<?php

return [
    'user' => 'Frontend user',
    'first_name' => 'First name',
    'last_name' => 'Last name',
    'sex' => 'Sex',
    'birth_date' => 'Birth date',
    'image' => 'image',
    'address' => 'Address',
    'activate' => 'Activate'
];