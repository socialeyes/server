<?php

return [
    'user' => 'User',
    'new' => 'Create new user',
    'name' => 'Name',
    'email' => 'Email',
    'sign_in' => 'Sign in',
    'password' => 'Password',
    'remember_me' => 'Remember me',
    'password_confirm' => 'Confirm password',
    'password_note' => '<strong>Note:</strong> Keep <em>Password</em> and <em>Confirm Password</em> blank if you do not want to change.',
    'profile' => 'Profile',
    'forgot_password' => 'I forgot my password'
];