<?php

return [
    'sign_in' => 'Sign in',
    'sign_out' => 'Sign out',
    'list' => 'List',
    'create' => 'Create',
    'cancel' => 'Cancel',
    'back_to_list' => 'Back to list',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'save' => 'Save',
    'are_you_sure' => 'Are you sure?',
    'not_found' => 'Not found',
    'save_success' => 'Saved successfully',
    'update_success' => 'Updated successfully',
    'delete_success' => 'Deleted successfully',
    'no' => 'No',
    'image' => 'Image',
    'close' => 'Close'
];