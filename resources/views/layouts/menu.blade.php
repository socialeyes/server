<li class="{{ Request::is('report_types*') ? 'active' : '' }}">
    <a href="{!! route('report_types.index') !!}">
        <i class="fa fa-th"></i><span>{{ __('main.list') }} {{ __('report_type.label') }}</span>
    </a>
</li>

<li class="treeview @if($activeMenu['main'] == 'user') active @endif">
    <a href="#">
        <i class="fa fa-users" aria-hidden="true"></i>
        <span>Users</span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li class="@if($activeMenu['sub'] == 'user-new') active @endif">
            <a href="{!! route('users.create') !!}">
                <i class="fa fa-plus" aria-hidden="true"></i> New
            </a>
        </li>

        <li class="@if($activeMenu['sub'] == 'user-list') active @endif">
            <a href="{!! route('users.index') !!}">
                <i class="fa fa-th" aria-hidden="true"></i> List
            </a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('report*') ? 'active' : '' }}">
    <a href="{!! route('reports.index') !!}">
        <i class="fa fa-th"></i><span>{{ __('main.list') }} {{ __('report.label') }}</span>
    </a>
</li>