@component('mail::message')
Thank you for creating account with {{ config('app.name') }}.

Please, use this verify code `{{ $content['verify_code'] }}` in order to complete your account registration.

The verify code will be expired in the next `{{ config('setting.activation.registration.expire_activated_code') / 60 }}` minutes

Thanks,<br>
{{ config('app.name') }}

@endcomponent