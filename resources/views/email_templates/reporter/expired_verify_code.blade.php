@component('mail::message')

The old verify code was expired. Here is your new verify code `{{ $content['verify_code'] }}`.
Please use this new verify code to complete your registration again.

The new verify code will be expired in the next `{{ config('setting.activation.registration.expire_activated_code') / 60 }}` minutes

Thanks,<br>
{{ config('app.name') }}

@endcomponent