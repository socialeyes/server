@component('mail::message')

Hi {{ $content['name'] }},

We got a request to reset your password.
Here is the code `{{ $content['verify_code'] }}` to renew your password.

If you do not request a password reset, please let us know {{ asset('/contact-us') }}

Thanks,<br>
{{ config('app.name') }}

@endcomponent