<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <title>Report Detail | SocialEye</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!-- Font awesome -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 30px;
                padding-top: 50px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .logo {
                width: 100px;
            }

            .img-report {
                width: 200px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref">
            <div class="top-right links">
                <a href="{{ url('/_api_docs_') }}" target="_blank">
                    <i class="fas fa-file"></i> API doc
                </a>
                |
                <a href="{{ url('/login') }}">
                    <i class="fas fa-key"></i> Backend Login
                </a>
            </div>

            <div class="content">
                <div class="title m-b-md">
                    <img class="logo" src="{!! asset('image/social_eye_logo.png') !!}" >
                    <br>
                    {{ config('app.name') }}
                </div>

                <div class="container">
                    <h1>Reporting Detail</h1>
                    <div class="row">
                        <div class="col-md-4 col-sm-offset-4">
                            <h2>Reporter</h2>
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td style="width: 100px;"><strong>Name</strong></td>
                                        <td>{!! $report->reporter->getDisplayName() !!}</td>
                                    </tr>

                                    <tr>
                                        <td style="width: 100px;" ><strong>Email</strong></td>
                                        <td>{!! $report->reporter->email !!}</td>
                                    </tr>

                                    <tr>
                                        <td style="width: 100px;"><strong>Sex</strong></td>
                                        <td>{!! $report->reporter->sex !!}</td>
                                    </tr>

                                    <tr>
                                        <td style="width: 100px;"><strong>Address</strong></td>
                                        <td>{!! $report->reporter->address !!}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-sm-offset-3">
                            <h2>Report about</h2>
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <td style="width: 150px;"><strong>Report Type</strong></td>
                                    <td>{!! $report->report_type->name !!}</td>
                                </tr>

                                <tr>
                                    <td style="width: 150px;"><strong>Audio</strong></td>
                                    <td>
                                        @if($report->audio)
                                            <video controls>
                                                <source src="{!! asset($report->audio) !!}" type="video/mp4">
                                                <p>Your browser doesn't support HTML5 video.
                                            </video>
                                        @endif
                                    </td>
                                </tr>

                                <tr>
                                    <td style="width: 150px;"><strong>Image</strong></td>
                                    <td>
                                        @if($report->image)
                                            <img src="{!! asset($report->image) !!}" class="img-report">
                                        @endif
                                    </td>
                                </tr>

                                <tr>
                                    <td style="width: 150px;"><strong>Description</strong></td>
                                    <td>{!! $report->description !!}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>
