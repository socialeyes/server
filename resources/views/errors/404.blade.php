<!DOCTYPE html>
<html lang="en">
    <head>
        <title>404 Page not found - {!! config('setting.static_data.siteTitle') !!}</title>
    </head>

    <body class="home page tennisclub_body body_style_wide body_filled article_style_stretch top_panel_show top_panel_above sidebar_hide">
        <div class="body_wrap">
            <div class="page_wrap">
                <div class="page_content_wrap page_paddings_no">
                    <div class="content_wrap">
                        <div class="content">
                            <div class="post_item post_item_single page type-page">
                                <div class="post_content">
                                    <div class="margin_bottom_50_imp padding_top_84_imp">
                                        <div class="sc_content content_wrap">
                                            <h3 class="sc_title sc_title_regular sc_align_center margin_top_null margin_bottom_tiny text_align_center font_weight_600 font_size_3_929em">404 - Page not found</h3>
                                            <h5 class="sc_title sc_title_regular sc_align_center ltr-spc margin_top_null text_align_center accent1 font_weight_400 font_size_0_857em">
                                                Uh Oh !
                                            </h5>
                                            <p class="text_align_center add_color_3 margin_bottom_4_286em_imp">
                                                Sorry, but the page you are looking for has not been found. Try checking the URL for errors then hit the button refresh on your browser.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </body>
</html>
