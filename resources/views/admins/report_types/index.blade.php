@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">
            {{ __('report_type.label') }}
        </h1>
        <br>
    </section>
    <div class="content">
        <div class="clearfix"></div>
        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                @include('admins.report_types.partials.table')
            </div>
        </div>
    </div>
@endsection

