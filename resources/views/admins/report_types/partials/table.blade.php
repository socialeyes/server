<table class="table table-responsive" id="reportTypes-table">
    <thead>
        <tr>
            <th>{{ __('main.no') }}</th>
            <th>{{ __('report_type.name') }}</th>
            <th>{{ __('report_type.image') }}</th>
        </tr>
    </thead>
    <tbody>
    @foreach($reportTypes as $key => $reportType)
        <tr>
            <td>{!! $key + 1 !!}</td>
            <td>{!! $reportType->name !!}</td>
            <td>
                @include('partials.image_lightbox', [
                    'id' => $reportType->id,
                    'image' => asset("$reportType->image"),
                    'smallImage' => true
                ])
            </td>
        </tr>
    @endforeach
    </tbody>
</table>