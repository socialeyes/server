@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">
            {{ __('main.list') }}{{ __('report.report_detail') }}
        </h1>
        <br>
    </section>
    <div class="content">
        <div class="clearfix"></div>
        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12 form-group">
                        <a href="{!! route('reports.index') !!}" class="btn btn-default">
                            <i class="fa fa-long-arrow-left" aria-hidden="true"></i> Back to Report list
                        </a>
                    </div>
                </div>

                @include('admins.reports.partials.show')
            </div>
        </div>
    </div>
@endsection

