<table class="table table-hover table-bordered table-striped table-responsive list-data">
    <thead>
    <tr>
        <th>{{ __('main.no') }}</th>
        <th>{{ __('report.reporter') }}</th>
        <th>{{ __('report.status') }}</th>
        <th>{{ __('report.authorize_report') }}</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($report->report_details as $key => $reportDetail)
        <tr class="odd gradeX">
            <td> {{ $key + 1 }}</td>
            <td>{!! $report->reporter->getDisplayName() !!}</td>
            <td>
                {!! Form::model($report, ['route' => ['reports.update', $reportDetail->id], 'method' => 'patch']) !!}
                    {!! Form::select('report_status_id', $reportStatuses, $reportDetail->report_status->id, ['class' => 'form-control']) !!}

                    <button type="submit" class="btn btn-primary">Update</button>
                {!! Form::close() !!}
            </td>
            <td>{!! $reportDetail->authorize_report->authorize_place->name !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>