<table class="table table-hover table-bordered table-striped table-responsive list-data">
    <thead>
    <tr>
        <th>{{ __('main.no') }}</th>
        <th>{{ __('report.reporter') }}</th>
        <th>{{ __('report.status') }}</th>
        <th>{{ __('report.report_type') }}</th>
        <th>{{ __('report.audio') }}</th>
        <th>{{ __('report.image') }}</th>
        <th>{{ __('report.description') }}</th>
        <th>{{ __('report.report_detail') }}</th>
    </tr>
    </thead>
    <tbody>
        @foreach ($reports as $key => $report)
            <tr class="odd gradeX">
                <td> {{ $key + 1 }}</td>
                <td>{!! $report->reporter->getDisplayName() !!}</td>
                <td>{!! $report->report_status->name !!}</td>
                <td>{!! $report->report_type->name !!}</td>
                <td>
                    <i class="fa fa-file-audio-o" aria-hidden="true"></i>
                </td>
                <td>
                    @include('partials.image_lightbox', [
                        'id' => 'report' . $key,
                        'image' => asset($report->image)
                    ])
                </td>
                <td>{!! $report->description !!}</td>
                <td>
                    <a href="{!! route('reports.show', $report->id) !!}" class="btn btn-default">
                        <i class="fa fa-th-list" aria-hidden="true"></i> {{ __('report.report_detail') }}
                    </a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
