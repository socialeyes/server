@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">
            {{ __('main.list') }}{{ __('user.user') }}
        </h1>
        <br>
    </section>
    <div class="content">
        <div class="clearfix"></div>
        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                @include('admins.users.partials.table')
            </div>
        </div>
    </div>
@endsection

