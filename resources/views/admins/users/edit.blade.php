@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            {{ __('main.edit') }}{{ __('user.user') }}
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                {!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}
                    @include('admins.users.partials.fields')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
