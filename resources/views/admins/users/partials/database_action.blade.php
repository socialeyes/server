{!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'delete']) !!}
	<div class='btn-group'>
	    <a href="{!! route('users.edit', [$user->id]) !!}" title="Edit" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
		{!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('" . trans('main.are_you_sure') . "')", 'title' => 'Delete']) !!}
	</div>
{!! Form::close() !!}
