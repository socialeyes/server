<table class="table table-hover table-bordered table-striped table-responsive list-data">
    <thead>
    <tr>
        <th>{{ __('main.no') }}</th>
        <th>{{ __('user.name') }}</th>
        <th>{{ __('user.email') }}</th>
        <th>
            <a href="{!! route('users.create') !!}" class="{{ __('main.create') }}">
                <i class="fa fa-plus" aria-hidden="true"></i> {{ __('main.create') }}
            </a>
        </th>
    </tr>
    </thead>
    <tbody>
        @foreach ($users as $key => $user)
            <tr class="odd gradeX">
                <td> {{ $key + 1 }}</td>
                <td>{!! $user->name !!}</td>
                <td>{!! $user->email !!}</td>
                <td>
                    @include('admins.users.partials.database_action', ['user' => $user])
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
