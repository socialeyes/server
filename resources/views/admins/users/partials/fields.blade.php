<div class="row">
    <div class="form-group col-sm-6">
        {!! Form::label('name', trans('user.name')) !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('email', trans('user.email'), ['class' => 'required']) !!}
        {!! Form::text('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-6">
        {!! Form::label('password', trans('user.password'), ['class' => isset($user) ? '' : 'required']) !!}
        <input type="password" minlength="8" @if(!isset($user)) required @endif class="form-control" name="password" value="">
        @if(isset($user))
            {!! trans('user.password_note') !!}
        @endif
    </div>

    <div class="form-group col-sm-6">
        {!! Form::label('confirm_password', trans('user.password_confirm'), ['class' => isset($user) ? '' : 'required']) !!}
        <input type="password" minlength="8" @if(!isset($user)) required @endif class="form-control " data-status="edit" name="confirm_password" value="">
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-12">
        @include('partials.admin.create_edit_action', ['route' => route('users.index')])
    </div>
</div>
