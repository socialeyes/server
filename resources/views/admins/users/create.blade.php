@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            {{ __('user.new') }}
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                {!! Form::open(['route' => 'users.store', 'enctype' => 'multipart/form-data']) !!}
                    @include('admins.users.partials.fields')
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
