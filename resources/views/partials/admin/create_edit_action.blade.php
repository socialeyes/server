<div class="form-group">
    <a href="{!! $route !!}" class="btn btn-default">
        <i class="fa fa-arrow-left" aria-hidden="true"></i> {{ __('main.back_to_list') }}
    </a>

    <button type="submit" class="btn btn-primary">
        <i class="fa fa-floppy-o" aria-hidden="true"></i> {{ __('main.save') }}
    </button>
</div>